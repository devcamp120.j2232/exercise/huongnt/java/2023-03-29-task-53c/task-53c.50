package models;
import interfaces.Movable;
public class MovableCircle implements Movable {
    private int radius;
    private MovablePoint center;

    //khởi tạo phương thức
    public MovableCircle(int radius, MovablePoint center) {
        this.radius = radius;
        this.center = center;
      }

      
    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius) {
        MovablePoint center = new MovablePoint(x, y, xSpeed, ySpeed);
    }

    @Override
    public String toString() {
        return "MovableCircle [radius=" + radius + ", center=" + center + "]";
    }

    @Override
    public void moveUp() {
        center.moveUp();

    }

    @Override
    public void moveDown() {
        center.moveDown();

    }

    @Override
    public void moveLeft() {
        center.moveLeft();

    }

    @Override
    public void moveRight() {
        center.moveRight();


    }
    
}
