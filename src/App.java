import models.MovablePoint;
import models.MovableCircle;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point1 = new MovablePoint(5, 6, 3, 4);
        MovablePoint point2 = new MovablePoint(1, 3, 4, 5);

        System.out.println("Point 1"+ point1.toString());
        System.out.println("Point 2"+ point2.toString());

        point1.moveUp();
        point1.moveUp();
        point1.moveLeft();

        System.out.println("Point 1 After move: "+ point1.toString());

        point2.moveDown();
        point2.moveDown();
        point2.moveRight();

        System.out.println("Point 2 After move: "+ point2.toString());

        MovableCircle circle = new MovableCircle(5, point1);
        circle.moveDown();
        circle.moveDown();
        circle.moveRight();

        System.out.println("Point 2 After move: "+ circle.toString());


    }
}
